xquery version "3.1";

module namespace iiifrest="https://textgrid.de/ns/iiif/rest";

import module namespace manifests="https://textgrid.de/ns/iiif/manifests" at "manifests.xqm";
import module namespace tgclient="https://sade.textgrid.de/ns/tgclient" at "modules/tgclient.xqm";
import module namespace rest="http://exquery.org/ns/restxq";

declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace tgmd="http://textgrid.info/namespaces/metadata/core/2010";

(: TILE related namespaces :)
declare namespace xlink="http://www.w3.org/1999/xlink";
declare namespace svg="http://www.w3.org/2000/svg";

declare variable $iiifrest:config := doc("config.xml")/config;
declare variable $iiifrest:iiif := $iiifrest:config/textgrid/iiif/string();
declare variable $iiifrest:tgcrud := $iiifrest:config/textgrid/crud/string();
declare variable $iiifrest:this-url := $iiifrest:config/url/string();
declare variable $iiifrest:login := xmldb:login("/db/apps", "admin", "");

(:declare variable $iiifrest:response-header := response:set-header("access-control-allow-origin", "*");:)

declare
  %rest:GET
  %rest:path("/iiif/{$token}/manifests/{$uri}")
  %output:method("json")
function iiifrest:manifests($uri as xs:string, $token as xs:string) {
  manifests:main($uri, $token)
};

declare
  %rest:GET
  %rest:path("/iiif/{$token}/image/{$uri}/info.json")
  %output:method("text")
function iiifrest:image-data($uri as xs:string, $token as xs:string) {
let $sid := local:sid-resolver($token)
let $request := local:get($uri, "info.json", $sid)
return
    util:base64-decode($request[2])
    => replace($iiifrest:iiif, $iiifrest:this-url || $token  || "/image/")
    => replace("textgrid:" || $uri, $uri)
};

declare
  %rest:GET
  %rest:path("/iiif/{$token}/image/{$uri}/{$part}/{$size}/{$rotation}/{$file}")
  %output:method("binary")
function iiifrest:image-data($uri as xs:string, $part as xs:string, $size as xs:string, $rotation as xs:string, $file as xs:string, $token as xs:string) {
let $sid := local:sid-resolver($token)
let $request := local:get($uri, $part || "/" || $size || "/" || $rotation || "/" || $file, $sid)
return
    $request[2]
};

declare
  %rest:GET
  %rest:path("/iiif/{$token}/manifests/{$tileUri}/annotation/{$imageUri}.json")
  %output:method("json")
function iiifrest:annotation($token as xs:string, $tileUri as xs:string, $imageUri as xs:string) {
  let $sid := local:sid-resolver($token)
  let $url := $iiifrest:tgcrud || "textgrid:" || $tileUri || "/data?sessionId=" || $sid
  let $tileData := hc:send-request(<hc:request href="{$url}" method="get"/>)[2] => parse-xml()
  let $annotations := $tileData//tei:link[starts-with(@targets, "#shape")]
  let $documents := map:new(
      for $doc in ($annotations/tokenize(@targets, " ")[starts-with(., "textgrid:")] ! substring-before(., "#"))
      let $url := $iiifrest:tgcrud || $doc || "/data?sessionId=" || $sid
      let $data := hc:send-request(<hc:request href="{$url}" method="get"/>)[2]
      return
          map:entry($doc, $data)
  )
  return

    map {
        "@context": "http://www.shared-canvas.org/ns/context.json",
        "@id": "textgrid:" || $tileUri,
        "@type": "sc:AnnotationList",
        "resources":
        array {
            for $annotation in $annotations
            let $targetToken := tokenize($annotation/@targets, " |#")[.!=""]
            let $shapeId := $targetToken[1]
            let $docUri := ($annotations/tokenize(@targets, " ")[starts-with(., "textgrid:")])[1] => substring-before("#")
            let $document := $documents( $docUri )
            let $text :=
                if(contains($annotation/@targets, "_start"))
                then
                    let $startId := $targetToken[contains(., "_start")]
                    let $endId := $targetToken[contains(., "_end")]
                    return
                        $document//tei:anchor[@xml:id=$startId]/following::text()[. << ./following::tei:anchor[@xml:id=$endId]]
                else
                    $document/*[@xml:id = substring-after($targetToken[last()], "#")]
            let $svgShape := $annotation/root()//svg:*[@id=$shapeId]
            return if($svgShape/local-name() != "rect") then () else
            let $width := $svgShape/parent::*/svg:image/@width => xs:int()
            let $height := $svgShape/parent::*/svg:image/@height => xs:int()
            let $x := ((substring-before($svgShape/@x, "%") => xs:float() * $width) div 100) => floor()
            let $y := ((substring-before($svgShape/@y, "%") => xs:float() * $height) div 100) => floor()
            let $w := ((substring-before($svgShape/@width, "%") => xs:float() * $width) div 100) => floor()
            let $h := ((substring-before($svgShape/@height, "%") => xs:float() * $height) div 100) => floor()
            let $xywh := string-join(($x, $y, $w, $h), ",")

            let $imageUri := substring-after($svgShape/parent::*/svg:image/@xlink:href, ":")
            return
                map {
                    "@context": "http://iiif.io/api/presentation/2/context.json",
                    "@id": "_:" || $shapeId,
                    "@type": "oa:Annotation",
                    "motivation": array{ "oa:describing", "oa:commenting"},
                    "resource": map{
                        "@type": "dctypes:Text",
                        "format": "text/plain",
                        "chars": serialize($text) => normalize-space()
                    },
                    "on": map{
                        "@type": "oa:SpecificResource",
                        "full": replace($iiifrest:this-url, "/iiif/", "/iiif/" || $token ) || "/manifests/" || $tileUri || "/canvas/" || $imageUri || ".json",
                        "selector": map{
                            "@type": "oa:FragmentSelector",
                            "value": "xywh=" || $xywh
                        },
                        "within": map{
                            "@id": $iiifrest:this-url || "manifests/" || $tileUri,
                            "@type": "sc:Manifest"
                        }
                    }
                }

        }
    }
};

declare
  %rest:GET
  %rest:path("/iiif/gettoken/{$uri}")
  %rest:header-param("X-textgrid-sid", "{$sid}")
  %output:method("json")
function iiifrest:token($sid as xs:string*, $uri as xs:string) {
    let $tguri := "textgrid:" || $uri
    let $meta := tgclient:getMeta($tguri, $iiifrest:tgcrud, $sid)
    let $tokens := doc("tokenstore.xml")/*
    let $uuid := util:uuid()
    let $img := iiifrest:first-img($tguri, $sid, $meta)
    let $timestamp := current-dateTime()
    let $title := $meta//tgmd:title/text()
    let $format := $meta//tgmd:format/text()
    let $store := update insert 
                    <token sid="{$sid}" token="{$uuid}" timestamp="{$timestamp}" title="{$title}" preview="{$img}" format="{$format}">
                        <uri>{$uri}</uri>
                    </token> into $tokens

    return map {
        "token": $uuid,
        "uri": $uri,
        "timestamp": $timestamp,
        "title": $title,
        "preview": $iiifrest:this-url || $uuid || "/image/" || substring-after($img, ":") || "/full/,500/0/default.jpg",
        "format": $format
    }

};


declare function iiifrest:first-img($tguri as xs:string, $sid as xs:string*, $meta) {

    if ($meta//tgmd:format/contains(., "tg.aggregation"))
        then
            iiifrest:first-img-from-agg($tguri, $sid)
        else if($meta//tgmd:format/contains(., "text/linkeditorlinkedfile"))
        then
            iiifrest:first-img-from-tile($tguri, $sid)
        else
            ()
};

declare function iiifrest:first-img-from-agg($uri as xs:string, $sid as xs:string*) {
    (tgclient:tgsearch-navigation-agg($uri, $sid)//tgmd:object[starts-with(.//tgmd:format, 'image/')])[1]//tgmd:textgridUri/text()
};

declare function iiifrest:first-img-from-tile($uri as xs:string, $sid as xs:string*) {
    tgclient:getData($uri, $iiifrest:tgcrud, $sid)//svg:image[1]/@xlink:href/string()
};

declare
  %rest:GET
  %rest:path("/iiif/list")
  %rest:header-param("X-textgrid-sid", "{$sid}")
  %output:method("json")
function iiifrest:list-available($sid as xs:string*) {
    let $tokens := doc("tokenstore.xml")//token[@sid eq $sid]
    return map { "manifests" : array {
        for $token in $tokens
            return map {
                "token": $token/@token/string(),
                "uri": $token/uri[1]/string(),
                "timestamp": $token/@timestamp/string(),
                "title": $token/@title/string(),
                "preview": $iiifrest:this-url || $token/@token/string() || "/image/" || substring-after($token/@preview/string(), ":") || "/full/,500/0/default.jpg",
                "format": $token/@format/string()
            }
        }
    }
};

declare
  %rest:DELETE
  %rest:path("/iiif/{$token}")
  %rest:header-param("X-textgrid-sid", "{$sid}")
  %output:method("json")
function iiifrest:remove-token($token as xs:string, $sid as xs:string*) {
    let $res := for $tok in doc("tokenstore.xml")//token[@sid eq $sid and @token eq $token]
        return
            update delete $tok
    return map {"deleted": $token}
};

declare %private function local:get($uri as xs:string, $sid as xs:string) {
  local:get($uri, "", $sid)
};

declare %private function local:get($uri as xs:string, $path as xs:string?,  $sid as xs:string) {
let $url := $iiifrest:iiif || "textgrid:" || $uri || ";sid=" || $sid || "/" || $path
let $log := util:log-system-out($url)
let $request := <hc:request method="get" href="{$url}" follow-redirect="true"/>
let $http := hc:send-request($request)
let $data := $http[2]
let $binary-data := try { $data => xs:base64Binary() }
                    catch * {
                        error(QName("err", "load1"), string-join(("&#10;Could not load data from TextGrid: ",$http,$data,$err:code,$err:description), "&#10;"))
                    }
return
    ($http[1], $binary-data)
};

declare %private function local:sid-resolver($token as xs:string) {
    string((doc("tokenstore.xml")//token[@token = $token])[1]/@sid)
};

