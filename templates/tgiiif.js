var sid="";
var uri="24g67";
var token;

var serviceUrl = window.location.host === 'sade.textgrid.de'
                                        ? 'https://sade.textgrid.de/iiif'
                                        : window.location.protocol + '//' + window.location.host + '/exist/restxq/iiif';

function setSid(lab) {
    sid=lab;
    log("SID: " + sid);
    getList();
}
function addTGObject(laburi, title, mime){
    uri = laburi.split(":")[1];
    log("URI: " + uri);
    gettoken(uri, title);
}

function gettoken(uri, title) {
    var data;
    var url = serviceUrl + '/gettoken/' + uri;
    log("url: " + url);
    var request = new XMLHttpRequest();
    request.open('GET', url, true);
     request.setRequestHeader("X-textgrid-sid", sid);
    request.onload = function() {
      if (request.status >= 200 && request.status < 400) {
        // Success!
        entry = JSON.parse(request.responseText);
        createLink(entry.uri, entry.token, entry.title, entry.preview, entry.format, entry.timestamp);
      } else {
        // We reached our target server, but it returned an error
      }
    };

    request.onerror = function() {
      // There was a connection error of some sort
      log("error fetching token. see " + request);
    };

    request.send();
}

function log(message){
    var p = document.createElement("p");
    var text = document.createTextNode(message);
    p.appendChild(text);
    document.getElementById("log").appendChild(p)
}

function getList() {
    var url = serviceUrl +'/list';
    var request = new XMLHttpRequest();
    request.open('GET', url, true);
    request.setRequestHeader("X-textgrid-sid", sid);
    request.onload = function() {
        data = request.responseText;
        log("list: " + data);
        list = JSON.parse(data);
        console.log(data);
        for(let i = 0; i < list.manifests.length; i++){
            var entry = list.manifests[i];
            createLink(entry.uri, entry.token, entry.title, entry.preview, entry.format, entry.timestamp);
        }
    }
    request.send();
}

function createLink(uri, token, title, img, format, timestamp) {
    var d1 = document.getElementById('iiitems');
    links = '<div class="card-image"><img src="'+img+'"></div>';
    links += '<div class="card-stacked">';
    links += '<div class="card-content">';
    links += '<span class="card-title activator grey-text text-darken-4">'+title + '</span>';
    links += '<p>'+format+'<br/><i class="material-icons">access_time</i>'+timestamp+'</p>';
    links += '<a href="#" class="btn-floating right" title="unshare" onclick="deleteToken(\''+token+'\'); return false;"><i class="material-icons">delete</i></a>'
    links += '</div>';
    links += '<div class="card-action">';
    links += '<a href="tify.html?uri='+uri+'&token='+token+'">TIFY</a>';
    links += '<a href="m3.html?uri='+uri+'&token='+token+'">Mirador</a>';
    links += '<a href="'+serviceUrl+'/'+token+'/manifests/'+uri+'">Manifest</a>';
    links += '</div></div>';
    d1.insertAdjacentHTML('beforeend', '<div id="'+token+'" class="card horizontal">'+links+'</div>');
}

function deleteToken(token) {
    var url = serviceUrl +'/' + token;
    var request = new XMLHttpRequest();
    request.open('DELETE', url, true);
    request.setRequestHeader("X-textgrid-sid", sid);
    request.onload = function() {
        var data = request.responseText;
        log("delete: " + data);
        var el = document.getElementById(token);
        el.parentNode.removeChild(el);
    }
    request.send();
}

function showLog() {
     document.getElementById("log").style.display = 'block';
}

