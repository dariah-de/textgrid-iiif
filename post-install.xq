let $module := xs:anyURI("/db/apps/iiifstream/rest.xqm")
return
    (
      (:  exrest:deregister-module($module), :)
        exrest:register-module($module)
    )
