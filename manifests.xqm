(:~
 : Module prepares IIIF manifest for resources stored at TextGrid
 : @author Mathias Göbel
 : @version 1.0.0
:)

xquery version "3.1";

module namespace manifests="https://textgrid.de/ns/iiif/manifests";

declare namespace exif="http://www.w3.org/2003/12/exif/ns#";
declare namespace ore="http://www.openarchives.org/ore/terms/";
declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace tgmd="http://textgrid.info/namespaces/metadata/core/2010";
declare namespace tgs="http://www.textgrid.info/namespaces/middleware/tgsearch";

(: TILE related namespaces :)
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace xlink="http://www.w3.org/1999/xlink";
declare namespace svg="http://www.w3.org/2000/svg";

(:~
 : Exposed function to prepare IIIF manifest
 : @param $resource a valid textgird URI without prefix, must be available in the tokenstore
 : @param $token a valid access token that will be checked against the tokenstore
:)
declare function manifests:main($resource as xs:string, $token as xs:string) {
let $tokenstore := doc("tokenstore.xml")
let $sid := string(($tokenstore//token[@token = $token])[1]/@sid)
return if($sid = "") then error(QName("error", "token"), "unable to resolve token! please get a new one using a valid sessionId.") else
let $config := doc("config.xml")/config
let $tgcrud := $config/textgrid/crud/string()
let $tgsearch := $config/textgrid/search/string()
let $this-url := $config/url/string()
let $this-url-manifests := $this-url || "manifests/" || $resource

let $url := $tgcrud || "textgrid:" || $resource || "/metadata?sessionId=" || $sid
let $tg-metadata := httpclient:get(xs:anyURI($url), false(), ())
let $format := string($tg-metadata//tgmd:format)

return if (contains($format, "tg.aggregation"))
then
    manifests:aggregation($tg-metadata, $tgsearch, $resource, $this-url-manifests, $tgcrud, $token)
else if ($format = "text/linkeditorlinkedfile")
    then
        let $object := let $url := $tgcrud || "textgrid:" || $resource || "/data?sessionId=" || $sid
                       return httpclient:get(xs:anyURI($url), false(), ())
        return
            manifests:text-image-link($object, $tg-metadata//tgmd:object, $this-url-manifests, $tgcrud, $token)
else
    error(QName("error", "format"), $format || "#" || $url || "#" || $token)

};

(:~ Prepares a manifest for a given TILE resource created by the Text-Image-Link-Editor
 : @param $object the Text-Image-Link root node
 : @param $tg-metadata TextGrid metadata file to the TILE object
 : @param $this-url-manifests the URL pointing to this manifest
 : @param $tgcrud TG-CRUD endpoint URL
 : @param $token a valid access token to be checked against the tokenstore
  :)
declare function manifests:text-image-link($object as element(), $tg-metadata as element(tgmd:object), $this-url-manifests as xs:string, $tgcrud as xs:string, $token as xs:string)
as map(*) {
let $sid := string((doc("tokenstore.xml")//token[@token = $token])[1]/@sid)
let $images := $object//svg:image/substring-after(@xlink:href, "textgrid:")
let $this-url-manifests := replace($this-url-manifests, "/iiif/", "/iiif/" || $token || "/")
return
    map{
        "within": "https://textgridrep.org/",
        "logo": "https://textgridrep.org/textgrid-theme/images/textgrid-repository-logo.svg",
        "@context": "http://iiif.io/api/presentation/2/context.json",
        "label": string($tg-metadata//tgmd:project) || ": " || string($tg-metadata//tgmd:title),
        "@type": "sc:Manifest",
        "@id":  replace($this-url-manifests, "/iiif/", "/iiif/" || $token || "/"),
        "description": "not available",
        "attribution": "rightsholder: not present for test data",
        "sequences": [
        	map {
    			"@id": $this-url-manifests || "/sequence/normal.json",
    			"@type": "sc:Sequence",
    			"label": "Current Page Order",
    			"viewingDirection": "left-to-right",
    			"viewingHint": "paged",
    			"canvases": array{(
    				for $image in $images
    				let $uri := $image
					  let $tokenstore := update insert <uri>{$uri}</uri> into (doc("tokenstore.xml")//token[@token = $token])[1]
    				let $url := $tgcrud || "textgrid:" || $uri || "/metadata?sessionId=" || $sid
            let $result := httpclient:get(xs:anyURI($url), false(), ())
            return
                map{
        					"@id":  $this-url-manifests || "/canvas/" || $uri || ".json",
        					"@type": "sc:Canvas",
        					"label": string($result//tgmd:title),
        					"height": number(($result//exif:height)[1]),
        					"width": number(($result//exif:width)[1]),
        					"images": [
        						map {
        							"@id": $this-url-manifests ||"/annotation/" || $uri || ".json",
        							"@type": "oa:Annotation",
        							"on": $this-url-manifests || "/canvas/" || $uri || ".json",
        							"motivation": "sc:painting",
        							"resource": map {
        								"@id": $tgcrud || string($result//tgmd:textgridUri) || "/data",
        								"@type": "dctypes:Image",
        								"format": string($result//tgmd:format),
        								"service": map {
        									"@id": substring-before($this-url-manifests, "manifests/") || "/image/" || $uri,
        									"profile": "http://iiif.io/api/image/2/level2.json",
        									"@context": "http://iiif.io/api/image/2/context.json"
        									},
        								"height": number(($result//exif:height)[1]),
        								"width": number(($result//exif:width)[1])
        								}
        							} ],
        							"otherContent": array{
        							    map{
        							        "@id": $this-url-manifests || "/annotation/" || $uri || ".json",
        							        "@type": "sc:AnnotationList"
        							    }
        							}
        				}
    			)}
    		}
    	]
    }
};

(:~ Prepares a manifest for a given aggregation resource created by the Text-Image-Link-Editor
 : @param $object the Text-Image-Link root node
 : @param $tg-metadata TextGrid metadata file to the TILE object
 : @param $this-url-manifests the URL pointing to this manifest
 : @param $tgcrud TG-CRUD endpoint URL
 : @param $token a valid access token to be checked against the tokenstore
  :)
declare function manifests:aggregation($tg-metadata as element(), $tgsearch as xs:string, $resource as xs:string, $this-url-manifests as xs:string, $tgcrud as xs:string, $token as xs:string)
as map(*) {
let $sid := string((doc("tokenstore.xml")//token[@token = $token])[1]/@sid)
let $aggregation := xs:anyURI($tgsearch || "/navigation/agg/textgrid:" || $resource || "?sid=" || $sid)
let $all-metadata := httpclient:get($aggregation, false(), ())
let $images := 	for $result in $all-metadata//tgs:result
			where starts-with($result//tgmd:format, "image/")
			where $result//exif:height
			where $result//exif:width
			let $uri := replace($result/@textgridUri, "textgrid:", "")
			let $tokenstore := update insert <uri>{$uri}</uri> into (doc("tokenstore.xml")//token[@token = $token])[1]
			return
				map{
					"@id":  $this-url-manifests || "/canvas/" || $uri || ".json",
					"@type": "sc:Canvas",
					"label": string($result//tgmd:title),
					"height": number(($result//exif:height)[1]),
					"width": number(($result//exif:width)[1]),
					"images": [
						map {
							"@id": $this-url-manifests ||"/annotation/" || $uri || ".json",
							"@type": "oa:Annotation",
							"resource": map {
								"@id": $tgcrud || string($result/@textgridUri) || "/data",
								"@type": "dctypes:Image",
								"format": string($result//tgmd:format),
								"service": map {
									"@id": substring-before($this-url-manifests, "manifests/") || $token || "/image/" || $uri,
									"profile": "http://iiif.io/api/image/2/level2.json",
									"@context": "http://iiif.io/api/image/2/context.json"
									},
								"height": number(($result//exif:height)[1]),
								"width": number(($result//exif:width)[1])
								},
							"on": $this-url-manifests || "/canvas/" || $uri || ".json",
							"motivation": "sc:painting"
							} ]
				}

    let $json :=
        map{
            "within": "https://textgridrep.org/",
            "logo": "https://textgridrep.org/textgrid-theme/images/textgrid-repository-logo.svg",
            "@context": "http://iiif.io/api/presentation/2/context.json",
            "label": string($tg-metadata//tgmd:project) || ": " || string($tg-metadata//tgmd:title),
            "@type": "sc:Manifest",
            "@id":  $this-url-manifests ||"/manifest.json",
            "description": "not available",
            "attribution": "rightsholder: not present for test data",
            "sequences": [
            	map {
    				"@id": $this-url-manifests || "/sequence/normal.json",
    				"@type": "sc:Sequence",
    				"label": "Current Page Order",
    				"viewingDirection": "left-to-right",
    				"viewingHint": "paged",
    				"canvases":
    					array{( $images )}
    			}
    		]
        }
    return
    	$json
};
